<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Experience<br>
 * Controller du table Experience
 */
class Experience extends CI_Controller
{
    
    function __construct() {
        parent::__construct();
        $this->load->model('experience_model', 'exp');
    }
    
}