<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Offer<br>
 * Controller du table Offer
 */
class Offer extends CI_Controller
{
    function __construct()
    {
      parent::__construct();
      $this->load->model('offer_model', 'offer');
    }

    /**
     * Fonction mitondra an'ny amin'ny liste an'ny offres disponibles miaraka amin'ny info motel sy room
     */
    public function available_offer_list()
    {
      $data['offer'] = $this->offer->available_offer_list();
      for ($i=0; $i < sizeof($data['offer']); $i++)
      {
        //alaina ny motel sy room_category isakin'ny offre
          $data['motel'][$i] = $this->offer->get_offer_s_motel($data['offer'][$i]['offer_id']);
          $data['room_category'][$i] = $this->offer->get_offer_s_room_category($data['offer'][$i]['offer_id']);
      }
      //redirection
      //+
      //Passage des données de $data vers la page available offer list
      //exemple $data['motel'] deviens $motel dans la vue available_offer_list
        $this->load->view('available_offer_list',$data);
    }

    /**
     * Fonction de redirection vers la page de recherche multi critères des offres
     */
    public function search_offer_page()
    {
      $this->load->model('motel_model');
      $this->load->model('room_category_model');
      $data = array();
      //atao anaty $data['motel'] ny motel rehetra
        $data['motel'] = $this->motel_model->get_Motels();
      //atao anay $data['room_category'] ny room_category rehetra
        $data['room_category'] = $this->room_category_model->get_Room_Categories();
        $this->load->view('search_offer_page',$data);
    }

    /**
     * Fonction apres validation du formulaire de recherche multi critères
     */
    public function search_offer()
    {
      //Load an'ny offer_model
      
      $data = array();
      $data['offer'] = array();
      $data['motel'] = array();
      $data['room_category'] = array();
      //ps: Tonga dia id an'ny motel sy id an'ny room_category no natao value tao amin'ny formulaire
      $id_motel = $_GET['motel'];
      $id_room_category = $_GET['room_category'];
      $max_price = $_GET['max_price'];
      //Atao none ny valeur an'ny variable raha tsy numerique ilay type an'ny variables azo avy amin'ny get
      if (!is_numeric($id_motel) OR $id_motel<=0)
      {
        $id_motel = "none";
      }
      if(!is_numeric($id_room_category) OR $id_room_category<=0)
      {
        $id_room_category = "none";
      }
      if(!is_numeric($max_price) OR $max_price<=0)
      {
        $max_price = "none";
      }
      //Condition si tout est Complet
      if($id_motel!="none" && $id_room_category!="none" && $max_price!="none")
      {
        $data['offer'] = $this->offer->get_Offers_By_Motel_And_Room_Category_And_Price($id_motel, $id_room_category,$max_price);
        for ($i=0; $i < sizeof($data['offer']); $i++)
        {
          $data['motel'][$i] = $this->offer->get_offer_s_motel($data['offer'][$i]['offer_id']);
          $data['room_category'][$i] = $this->offer->get_offer_s_room_category($data['offer'][$i]['offer_id']);
        }
        //Redirection vers la vue result_search
        $this->load->view('result_search', $data);
      }
      //Condition si tout est vide
      if($id_motel=="none" && $id_room_category=="none" && $max_price=="none")
      {
        $data['offer'] = $this->offer->get_offers();
        for ($i=0; $i < sizeof($data['offer']); $i++)
        {
          $data['motel'][$i] = $this->offer->get_offer_s_motel($data['offer'][$i]['offer_id']);
          $data['room_category'][$i] = $this->offer->get_offer_s_room_category($data['offer'][$i]['offer_id']);
        }
        //Redirection vers la vue result_search
        $this->load->view('result_search', $data);
      }

      //Condition si le motel est vide
      if($id_motel=="none" && $id_room_category!="none" && $max_price!="none")
      {
        $data['offer'] = $this->offer->get_Offers_By_Room_Category_And_Price($id_room_category,$max_price);
        for ($i=0; $i < sizeof($data['offer']); $i++)
        {
          $data['motel'][$i] = $this->offer->get_offer_s_motel($data['offer'][$i]['offer_id']);
          $data['room_category'][$i] = $this->offer->get_offer_s_room_category($data['offer'][$i]['offer_id']);
        }
        //Redirection vers la vue result_search
        $this->load->view('result_search', $data);
      }

      //Condition si le room_category est vide
      if($id_motel!="none" && $id_room_category=="none" && $max_price!="none")
      {
        $data['offer'] = $this->offer->get_Offers_By_Motel_And_Price($id_motel, $max_price);
        for ($i=0; $i < sizeof($data['offer']); $i++)
        {
          $data['motel'][$i] = $this->offer->get_offer_s_motel($data['offer'][$i]['offer_id']);
          $data['room_category'][$i] = $this->offer->get_offer_s_room_category($data['offer'][$i]['offer_id']);
        }
        //Redirection vers la vue result_search
        $this->load->view('result_search', $data);
      }

      //Condition si le max_price est vide
      if($id_motel!="none" && $id_room_category!="none" && $max_price=="none")
      {
        $data['offer'] = $this->offer->get_Offers_By_Motel_And_Room_Category($id_motel, $id_room_category);
        for ($i=0; $i < sizeof($data['offer']); $i++)
        {
          $data['motel'][$i] = $this->offer->get_offer_s_motel($data['offer'][$i]['offer_id']);
          $data['room_category'][$i] = $this->offer->get_offer_s_room_category($data['offer'][$i]['offer_id']);
        }
        //Redirection vers la vue result_search
        $this->load->view('result_search', $data);
      }

      //Condition si le motel et le room_category sont vides
      if($id_motel=="none" && $id_room_category=="none" && $max_price!="none")
      {
        $data['offer'] = $this->offer->get_Offers_By_Price($max_price);
        for ($i=0; $i < sizeof($data['offer']); $i++)
        {
          $data['motel'][$i] = $this->offer->get_offer_s_motel($data['offer'][$i]['offer_id']);
          $data['room_category'][$i] = $this->offer->get_offer_s_room_category($data['offer'][$i]['offer_id']);
        }
        //Redirection vers la vue result_search
        $this->load->view('result_search', $data);
      }

      //Condition si le motel et le max_price sont vides
      if($id_motel=="none" && $id_room_category!="none" && $max_price=="none")
      {
        $data['offer'] = $this->offer->get_Offers_By_Room_Category($id_room_category);
        for ($i=0; $i < sizeof($data['offer']); $i++)
        {
          $data['motel'][$i] = $this->offer->get_offer_s_motel($data['offer'][$i]['offer_id']);
          $data['room_category'][$i] = $this->offer->get_offer_s_room_category($data['offer'][$i]['offer_id']);
        }
        //Redirection vers la vue result_search
        $this->load->view('result_search', $data);
      }

      //Condition si le room_category et le max_price sont vides
      if($id_motel!="none" && $id_room_category=="none" && $max_price=="none")
      {
        $data['offer'] = $this->offer->get_Offers_By_Motel($id_motel);
        for ($i=0; $i < sizeof($data['offer']); $i++)
        {
          $data['motel'][$i] = $this->offer->get_offer_s_motel($data['offer'][$i]['offer_id']);
          $data['room_category'][$i] = $this->offer->get_offer_s_room_category($data['offer'][$i]['offer_id']);
        }
        //Redirection vers la vue result_search
        $this->load->view('result_search', $data);
      }

    }
}
?>
