<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Motel<br>
 * Controller du table Motel
 */
class Motel extends CI_Controller
{
    
    function __construct() {
        parent::__construct();
        $this->load->model('motel_model', 'motel');
    }

}
