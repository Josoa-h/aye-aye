<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Client<br>
 * Controller du table client
 */
class Client extends CI_Controller
{

    function __construct() {
        parent::__construct();
        $this->load->model('client_model', 'client');
    }


}