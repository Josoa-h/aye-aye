<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Motel_model extends CI_Model
{
  // Fonction mamerina ny motel rehetra
  public function get_Motels() : array
  {
    $i = 0;
    $datas = array();
    $query = $this->db->query('SELECT * from motel');
    foreach ($query -> result_array() as $value)
    {
      $datas[$i] = $value;
      $i++;
    }
    return $datas;
  }
  // Fonction mamerina ny motel arakarakin'ny motel_id
  public function get_Motel_By_Id($id) : array
  {
    $datas = array();
    $query = $this->db->query('SELECT * from motel WHERE motel_id= '.$id.'');
    foreach ($query-> result_array() as $value)
    {
      $datas = $value;
    }
    return $datas;
  }

  /**
     * Fonction recherche
     * @param string $recherche 
     
     * @return array resultats.
     */
  public function search($recherche) : array
  {
    $query=$this->db->query("SELECT * FROM motel WHERE name LIKE '%".$recherche."%' OR location LIKE '%".$recherche."%'");
    return $query->result_array();
  }
}
