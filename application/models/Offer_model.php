<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Offer_model extends CI_Model
{
  //Fonction mamerina ny offres rehetra
  public function get_Offers() : array
  {
    $i=0;
    $offer = array();
    $query = $this->db->query('SELECT * FROM offer');
    foreach ($query->result_array() as $value)
    {
      $offer[$i] = $value;
      $i++;
    }
    return $offer;
  }
  //fonction mamerina ny offre correspondant amin'ny ID any
  public function get_Offer_By_Id($offer_id) : array
  {
    $offer = array();
    $query = $this->db->query('SELECT * FROM offer WHERE offer_id='.$offer_id.'');
    foreach ($query-> result_array() as $value)
    {
      $offer = $value;
    }
    return $offer;
  }
  //Fonction mamerina ny offres correspondant amin'ny Motel
  public function get_Offers_By_Motel($motel_id) : array
  {
    $i = 0;
    $offer = array();
    $query = $this->db->query('SELECT * FROM offer WHERE motel_id LIKE '.$motel_id.'');
    foreach ($query->result_array() as $value)
    {
      $offer[$i] = $value;
      $i++;
    }
    return $offer;
  }

  //Fonction mamerina ny offre correspondant amin'ny ID an'ny room_category
  public function get_Offers_By_Room_Category($room_category_id) : array
  {
    $i = 0;
    $offer = array();
    $query = $this->db->query('SELECT * FROM offer WHERE room_category_id LIKE '.$room_category_id.'');
    foreach ($query->result_array() as $value)
    {
      $offer[$i] = $value;
      $i++;
    }
    return $offer;
  }

  //Fonction mamerina ny offre correspondant amin'ny prix max
  public function get_Offers_By_Price($price) : array
  {
    $i = 0;
    $offer = array();
    $query = $this->db->query('SELECT * FROM offer WHERE price<='.$price.'');
    foreach ($query->result_array() as $value)
    {
      $offer[$i] = $value;
      $i++;
    }
    return $offer;
  }

  //Fonction mamerina ny offre correspondant amin'ny Room_category sy prix max
  public function get_Offers_By_Room_Category_And_Price($room_category_id, $price) : array
  {
    $i = 0;
    $offer = array();
    $query = $this->db->query('SELECT * FROM offer WHERE room_category_id LIKE '.$room_category_id.' AND price<='.$price.'');
    foreach ($query->result_array() as $value)
    {
      $offer[$i] = $value;
      $i++;
    }
    return $offer;
  }

  //Fonction mamerina ny offre correspondant amin'ny Motel sy Prix
  public function get_Offers_By_Motel_And_Price($motel_id, $price) : array
  {
    $i = 0;
    $offer = array();
    $query = $this->db->query('SELECT * FROM offer WHERE motel_id LIKE '.$motel_id.' AND price<='.$price.'');
    foreach ($query->result_array() as $value)
    {
      $offer[$i] = $value;
      $i++;
    }
    return $offer;
  }

  //Fonction mamerina ny offre correspondant amin'ny Motel sy room_category
  public function get_Offers_By_Motel_And_Room_Category($motel_id, $room_category_id) : array
  {
    $i = 0;
    $offer = array();
    $query = $this->db->query('SELECT * FROM offer WHERE motel_id LIKE '.$motel_id.' AND room_category_id LIKE '.$room_category_id.'');
    foreach ($query->result_array() as $value)
    {
      $offer[$i] = $value;
      $i++;
    }
    return $offer;
  }

  //Fonction mamerina ny offre correspondant amin'ny Motel sy room_category sy price
  public function get_Offers_By_Motel_And_Room_Category_And_Price($motel_id, $room_category, $price) : array
  {
    $i = 0;
    $offer = array();
    $query = $this->db->query('SELECT * FROM offer WHERE motel_id LIKE '.$motel_id.' AND room_category_id LIKE '.$room_category.' AND price<='.$price.'');
    foreach ($query->result_array() as $value)
    {
      $offer[$i] = $value;
      $i++;
    }
    return $offer;
  }

  //Fonction mamerina ny liste anah offre disponible
  public function available_offer_list() : array
  {
    $i = 0;
    $datas = array();
    $query = $this->db->query('SELECT * FROM offer WHERE is_available=1');
    foreach ($query -> result_array() as $value)
    {
      $datas[$i] = $value;
  //    echo $datas[$i]['offer_id'];
      $i++;
    }
    return $datas;
  }
  //Fonction maka ny motel anaty offre anakiray (paramètre ny id anleh offre)
  public function get_offer_s_motel($offer_id)
  {
    $offer = $this->get_Offer_By_Id($offer_id);
    $this->load->model('motel_model');
    $motel = $this->motel_model->get_Motel_By_Id($offer['motel_id']);
    return $motel;
  }

  //Fonction maka ny room_category anaty offre anakiray (paramètre ny id anleh offre)
  public function get_offer_s_room_category($offer_id)
  {
    $offer = $this->get_Offer_By_Id($offer_id);
    $this->load->model('room_category_model');
    $room_category = $this->room_category_model->get_Room_Category_By_Id($offer['room_category_id']);
    return $room_category;
  }
}
?>
