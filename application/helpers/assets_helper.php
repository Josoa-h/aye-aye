<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
     * @param mixed $url = nomFichier dans le dossier assets/css/
     * 
     * @return string = assets/css/(nomFichier).css
     */
if(!function_exists('css_url')){
    function css_url($url) : string{
        return "assets/css/".$url.".css";
    }
}

/**
     * @param mixed $url = nomFichier dans le dossier assets/js/
     * 
     * @return string = assets/js/(nomFichier).js
     */
    if(!function_exists('js_url')){
        function js_url($url) : string{
            return "assets/js/".$url.".js";
        }
    }