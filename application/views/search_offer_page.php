<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
  /*
    PAGE TEST AN'NY RECHERCHE MULTI CRITERE
  */
 ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Search offer page</title>
  </head>
  <body>
    <form action="<?php echo site_url('offer/search_offer'); ?>" method="get">
    <strong>Motel : </strong>
    <select name="motel">
        <option value="none"></option>
        <?php
          for ($i=0; $i < sizeof($motel); $i++) {
            ?>
              <option value="<?php echo $motel[$i]['motel_id']; ?>"><?php echo $motel[$i]['name']; ?></option>
            <?php
          }
         ?>
      </select></p>
      <strong>Room Category :</strong>
      <select name="room_category">
        <option value="none"></option>
        <?php
          for ($i=0; $i<sizeof($room_category); $i++)
          {
            ?>
              <option value="<?php echo $room_category[$i]['room_category_id']; ?>"><?php echo $room_category[$i]['name']; ?></option>
            <?php
          }
         ?>
      </select><br></p>
      <p>
          <strong>Max-Price : </strong>
          <input type="text" name="max_price">
      </p>
      <button type="submit">Submit</button>
    </form>
  </body>
</html>
